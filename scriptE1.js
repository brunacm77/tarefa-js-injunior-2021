
console.log("----Exercicio 1 -----");
console.log("Array com 5 elementos de 0 a 100");

let array = [];

for (i = 0; i < 5; i++) {
    array.push(Math.floor(Math.random() * 100));
}
console.log(array);

console.log("Array com 5 elementos ordenado:");

let aux = 0;

for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
        if (array[i] < array[j]) {
            aux = array[i];
            array[i] = array[j];
            array[j] = aux;
        }
    }
}
console.log(array);